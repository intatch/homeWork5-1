const fs = require('fs')

const classifyEyeColor = (stat, JSONpersons) => {
    if(JSONpersons.eyeColor === "brown"){
        stat.brown +=1
    }   
    if(JSONpersons.eyeColor === "green"){
        stat.green +=1
    }   
    if(JSONpersons.eyeColor === "blue"){
        stat.blue +=1
    }
    return stat
}

const classifyGerder = (stat, JSONpersons)=>{
    if(JSONpersons.gender === "male"){
        stat.male += 1
    }
    else{
        stat.female += 1
    }
    return stat
}

const countFriends =  (JSONpersons)=>{
    let personIdandFriends = {}
    personIdandFriends._id = JSONpersons._id
    personIdandFriends.countFriend = JSONpersons.friends.length
    return personIdandFriends
}




fs.readFile('homework1-4.json', 'utf8', (err, data) => {
    let JSONpersons = JSON.parse(data)

    let countEyeColors = JSONpersons.reduce(classifyEyeColor, {brown: 0,green: 0,blue: 0})
    let countGender = JSONpersons.reduce(classifyGerder, {male:0 ,female:0})
    let countFriens = JSONpersons.map(countFriends)



    let strEyeColor = JSON.stringify(countEyeColors)    
    let strGender = JSON.stringify(countGender)   
    let strPersons = JSON.stringify(countFriens)   

    fs.writeFile('homework5-1_eyes.json', strEyeColor, 'utf8', (err) => {
        if(err){
            console.log(err)
        }
    })

    fs.writeFile('homework5-1_gender.json', strGender, 'utf8', (err) => {
        if(err){
            console.log(err)
        }
    })

    fs.writeFile('homework5-1_friends.json', strPersons, 'utf8', (err) => {
        if(err){
            console.log(err)
        }
    })

})

let readFileFunc = (nameFile, done) => {
    fs.readFile(nameFile, 'utf8', (err, data)=>{
        if(err){
            done(err,null)
        }
        else{
            done(null, data)
        }
    })
}

exports.readFileFunc = readFileFunc