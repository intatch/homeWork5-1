const assert = require('assert');
const fs = require('fs')

const code = require('./code.js')

let readFileFunc = code.readFileFunc

describe('tdd lab', function () {
    describe('#checkFileExist()', function () {
        it('should have homework5-1_eyes.json existed', (done)=> {
            let nameFile = 'homework5-1_eyes.json'
            readFileFunc(nameFile,done)
        });
        it('should have homework5-1_gender.json existed', (done) => {
            let nameFile = 'homework5-1_gender.json'
            readFileFunc(nameFile,done)
        });
        it('should have homework5-1_friends.json existed', (done) => {
            let nameFile = 'homework5-1_friends.json'
            readFileFunc(nameFile,done)
        });
    });
    describe('#objectKey()', function () {

        it('should have same object key stucture as homework5-1_gender.json', (done) => {
            let nameFile = 'homework5-1_gender.json'
            let objGender = {"male":11,"female":12}
            readFileFunc(nameFile, (err, data) => {
                let objData = JSON.parse(data)
                if(err){
                    done(err)
                }else{
                    assert.deepEqual(objData, objGender);
                    done()
                }
            })
        });
        it('should have same object key stucture as homework5-1_friends.json', (done) => {
            let nameFile = 'homework5-1_friends.json'
            let objFriends = [{"_id":"5a3711070776d02ed87d2100","countFriend":3},{"_id":"5a371107493db1e1fd7a153f","countFriend":1},{"_id":"5a3711079f44c94b32f42b0b","countFriend":3},{"_id":"5a3711071f8377f04dcc0647","countFriend":0},{"_id":"5a3711072e74ea6f409fdb43","countFriend":2},{"_id":"5a37110722a47e68e57f142f","countFriend":3},{"_id":"5a371107066b63e31205f928","countFriend":1},{"_id":"5a371107d29f5b88763ed067","countFriend":3},{"_id":"5a371107e7cb282674fa91ba","countFriend":0},{"_id":"5a37110765fb042dc2916d9d","countFriend":1},{"_id":"5a371107e92ac1efaa56a45b","countFriend":3},{"_id":"5a3711076224800cc138e3c5","countFriend":1},{"_id":"5a371107cbb692e1ccd27ded","countFriend":1},{"_id":"5a37110708e52d671991d276","countFriend":0},{"_id":"5a371107208b81a965d234bd","countFriend":1},{"_id":"5a37110767113b570288b660","countFriend":2},{"_id":"5a37110753339359bb11244f","countFriend":1},{"_id":"5a3711072da924792a4b5cdf","countFriend":0},{"_id":"5a37110791bdc1ee45753892","countFriend":0},{"_id":"5a3711075fb9f745fc1eea7f","countFriend":3},{"_id":"5a371107b8dedc7ce188485a","countFriend":3},{"_id":"5a371107de6e9c5992f616db","countFriend":2},{"_id":"5a3711076adf9da3c64e4beb","countFriend":2}] 
            readFileFunc(nameFile, (err, data) => {
                let objData = JSON.parse(data)
                if(err){
                    done(err)
                }else{
                    assert.deepEqual(objData, objFriends);
                    done()
                }
            })
        });
        it('should have same object key stucture as homework5-1_eyeColor.json', (done) => {
            let nameFile = 'homework5-1_eyes.json'
            let objGender = {"brown":11,"green":6,"blue":6}
            readFileFunc(nameFile, (err, data) => {
                let objData = JSON.parse(data)
                if(err){
                    done(err)
                }else{
                    assert.deepEqual(objData, objGender);
                    done()
                }
            })
        });
    });
    describe('#userFriendCount()', function () {
        it('should have size of array input as 23', (done) => {
            let nameFile = 'homework5-1_friends.json'
            let sizePersons = 23
            readFileFunc(nameFile, (err, data) => {
                let objData = JSON.parse(data)
                let countobjData = objData.length
                if(err){
                    done(err)
                }else{
                    assert.deepEqual(countobjData, sizePersons);
                    done()
                }
            })
        });
    });
    describe('#sumOfEyes()', function () {
        it('should have sum of eyes as 23', (done) => {
            let sizePersons = 23
            let nameFile = 'homework5-1_eyes.json'
            readFileFunc(nameFile, (err, data) => {
                let objData = JSON.parse(data)
                let arrValuesData = Object.values(objData)
                let sumPersons = arrValuesData.reduce((sum, arrValuesData)=>{
                    return sum + arrValuesData
                }, 0)
                if(err){
                    done(err)
                }else{
                    assert.deepEqual(sumPersons, sizePersons);
                    done()
                }
            })
        });
    });

    describe('#sumOfGender()', function () {
        it('should have sum of gender as 23', (done) => {
            let sizePersons = 23
            let nameFile = 'homework5-1_gender.json'
            readFileFunc(nameFile, (err, data) => {
                let objData = JSON.parse(data)
                let arrValuesData = Object.values(objData)
                let sumPersons = arrValuesData.reduce((sum, arrValuesData)=>{
                    return sum + arrValuesData
                }, 0)
                if(err){
                    done(err)
                }else{
                    assert.deepEqual(sumPersons, sizePersons);
                    done()
                }
            })
        });
    });
});